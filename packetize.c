/*
 * packetize.c
 *
 * Created 1/2011 by spc
 * Version 0.2
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "packetize.h"

//#define DBG

// createPacket - allocate a new Packet and copy in given char array
// Pre: 0 < length of contents <= PACKET_SIZE
struct Packet *createPacket(char *contents) {
  struct Packet *pkt = (struct Packet *) malloc(sizeof(struct Packet));

  // check for failed allocation 
  if (pkt == NULL) {
    return NULL;
  } 

  int size = initPacket(pkt, contents);

  if (size == 0) { // size is 0, initialization failed
    free(pkt);
    pkt = NULL;
  }

  return pkt;
  // this function implicitly requires the user to free up the allocated space
}

// initPacket - initialize an existing Packet with a char array
// Pre: 0 < length of contents <= PACKET_SIZE
// Post: initializes pkt from contents and returns packet length
int initPacket(struct Packet *pkt, char *contents) {
  int i, len = strlen(contents);

  if (len == 0 || len > PACKET_SIZE) {
    return 0;
  } else {
    pkt->packet_length = len;
  }

#ifdef DBG
  printf("initPacket [%d]: ", len);
#endif
  for (i = 0; i < len; i++) {
    pkt->packet[i] = contents[i];
#ifdef DBG
    printf("%c", contents[i]);
#endif
  }
  // possible substitution for the above: memcpy
#ifdef DBG
  printf("\n");
#endif

  return len;
}

// Question - why doesn't the return value of createPacket become invalid once the function terminates?

// packet2string - return a character array containing information in pkt
// Post: returns newly allocated array of characters in packet, or NULL if error
char *packet2string(struct Packet *pkt) {
  char *pkt_str = (char *) malloc(pkt->packet_length + 1);
  if (pkt_str != NULL) {
    memcpy(pkt_str, pkt->packet, pkt->packet_length);
    pkt_str[pkt->packet_length] = '\0';
  }
  // we don't need to check for a valid pkt address, we pushed that requirement onto the user
  // note that we also gave the requirement to free the dynamic memory to the user
  return pkt_str;
}

// packetize - return an array of packets containing the original message
// Post: returns newly allocated array of packets, or NULL if error
struct MessagePackets *packetize(char *message) {
  char *chunk;
  int len = strlen(message);
  int num_pkts = len / PACKET_SIZE + 1;
  int i, index = 0;
	
#ifdef DBG
  printf("packetize: [%d]\n", num_pkts);
#endif
  struct MessagePackets *mp = (struct MessagePackets *) malloc(sizeof(struct MessagePackets));
  if (mp == NULL) return mp;

  // create a dynamic array OF POINTERS to packets
  struct Packet **packets = (struct Packet **) malloc(sizeof(struct Packet *) * num_pkts);
  if (packets == NULL) { 
    return NULL;
  } else {
    mp->num_packets = num_pkts;
    mp->packets = packets;
  }

  // break the message into PACKET_SIZEd chunks
  chunk = (char *) malloc(PACKET_SIZE+1);    
  for (i = 0; i < num_pkts-1; i++) {  // THE MINUS 1 IS IMPORTANT HERE!
    // grab next chunk (PACKET_SIZE elements of message) and put in array
    strncpy(chunk, message+index, PACKET_SIZE);

    // create new Packet with chunk
    packets[i] = createPacket(chunk);
    index += PACKET_SIZE;
  }
	
  // check for leftover characters to make the last packet
  if (index < len) {
    strncpy(chunk, message+index, len-index);
    // safe side, add a nul-terminator
    chunk[len-index] = '\0';
#ifdef DBG
    printf("last chunk: [%d]\n%s", len-index, chunk);
#endif
    packets[num_pkts-1] = createPacket(chunk);
  }
  free(chunk);

  return mp;
}

// Design decision here - do we want a deep or shallow copy???
// Shallow copy is easier, but assumes that the MessagePackets object will persist
// as long as the returned packet is needed.  Deep copy is a bit more tedious to 
// write, but decouples the returned packet from the host data structure.  A good
// software engineer practices decoupling.

// getPacket - returns copy of requested packet
// Pre: mp refers to a valid MessagePackets object && 0 <= index < num_packets
// Post: returns reference to requested packet or NULL if error
struct Packet *getPacket(struct MessagePackets *mp, int index) {
  struct Packet *p = NULL;

  if (index >= 0 && index < mp->num_packets) {
    p = (struct Packet *) malloc(sizeof(struct Packet));
    if (p == NULL) return p;
    p->packet_length = (mp->packets[index])->packet_length;
    memcpy(p->packet, (mp->packets[index])->packet, p->packet_length);
#ifdef DBG
  printf("getPacket: [%d]\n", p->packet_length);
#endif
  }

  return p;
}
