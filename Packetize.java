import java.util.*;

// Class Packet: stores one chunk of a message as a packet (by default, a 512 byte array of char)

class Packet {

    // Attributes
    static final int PACKET_SIZE = 512;

    char[] packet = new char[PACKET_SIZE];

    public Packet(String contents) {
	int len = contents.length();

	if (len > PACKET_SIZE) {
	    throw new IllegalArgumentException();
	}
	
	for (int i = 0; i < len; i++) {
	    packet[i] = contents.charAt(i);
	}
    }
    
    public String toString() {
	return new String(packet);
    }
}

// Class Packetize: when an object of this class is created, a message string is
// broken into (mostly) equal sized chunks (packets) and stored as an array.  Individual 
// packets are accessed by index using the getPacket method.

public class Packetize {

    Packet[] packets;

    public Packetize(String message) {
	String chunk;
	int len = message.length();
	int num_pkts = len / Packet.PACKET_SIZE + 1;
	int index = 0;
	
	// create enough packets to hold entire message
	packets = new Packet [num_pkts];

	// break the message into PACKET_SIZEd chunks
	for (int i = 0; i < num_pkts-1; i++) {  // THE MINUS 1 IS IMPORTANT HERE!
	    // grab next chunk (PACKET_SIZE elements of message) and put in array
	    chunk = message.substring(index, index+512);

	    // create new Packet with chunk
	    packets[i] = new Packet(chunk);
	    index += 512;
	}
	
	// check for leftover characters to make the last packet
	if (index < len) {
	    chunk = message.substring(index, len);
	    packets[num_pkts-1] = new Packet(chunk);
	}
    }

    public Packet getPacket(int index) {
	return packets[index];      // relies on built-in bounds checker to test index
    }

    public static void main(String[] args) {
	// simple test for Packetize
	String alpha = "There's a lady who's sure, all that glitters is gold / And she's buying a stairway to heaven / When she gets there she knows, if the stores are all closed / With a word she can get what she came for / Ooo and she's buying a stairway to heaven / There's a sign on the wall, but she wants to be sure / 'cause you know sometimes words can have two meanings / And it makes me wonder... / If there's a bustle by your hedgerow, don't be alarmed now / It's just a spring-clean for the Mayqueen / Yes there are two paths you can go by, but in the long run / There's still time to change the road you're on / And it makes me wonder, ooo really makes me wonder";

	System.out.println(alpha.length());

	Packetize me = new Packetize(alpha);
	System.out.println(me.getPacket(0));
	System.out.println(me.getPacket(1));
    }
	
}
