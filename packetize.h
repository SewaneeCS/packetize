/*
 * packetize.h
 *
 * Created 1/2011 by spc
 * Version 0.2
 */

// Defines the default size of a single packet
#define PACKET_SIZE 512

// Packet: stores one chunk of a message as a packet (by default, a 512 byte array of char)

struct Packet {
  int packet_length;
  char packet[PACKET_SIZE];
};

// MessagePackets: an array of Packets holding a complete message

struct MessagePackets {
  int num_packets;
  struct Packet **packets;
};

int initPacket(struct Packet *, char *);
struct Packet *createPacket(char *);
char *packet2string(struct Packet *);
struct MessagePackets *packetize(char *);
struct Packet *getPacket(struct MessagePackets *, int);
