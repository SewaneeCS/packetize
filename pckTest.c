/*
 * pckTest.c
 *
 * Created 1/2011 by spc
 * Version 0.2
 *
 * Compile with: gcc -Wall -o pktTest packetize.c pckTest.c
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "packetize.h"

// alpha test for Packetize: 2 packets, 650 total characters
char *alpha = "There's a lady who's sure, all that glitters is gold / And she's buying a stairway to heaven / When she gets there she knows, if the stores are all closed / With a word she can get what she came for / Ooo and she's buying a stairway to heaven / There's a sign on the wall, but she wants to be sure / 'cause you know sometimes words can have two meanings / And it makes me wonder... / If there's a bustle by your hedgerow, don't be alarmed now / It's just a spring-clean for the Mayqueen / Yes there are two paths you can go by, but in the long run / There's still time to change the road you're on / And it makes me wonder, ooo really makes me wonder";


int main(int argc, char *argv[]) {
  char *useMe = "you know what, shut up";

  if (argc == 2) {
    scanf("%s", useMe);
  } else { 
    useMe = alpha;
  }

  printf("Args: %d Msg length: %d\n", argc, strlen(useMe));

  struct MessagePackets *mp = packetize(useMe);
  
  struct Packet *pack1, *pack2;
  printf("First two packets:\n\n");
  pack1 = getPacket(mp, 0);
  pack2 = getPacket(mp, 1);
  
  printf("[%s]\n", packet2string(pack1));
  printf("[%s]\n", packet2string(pack2));

  free(pack1);
  free(pack2);
  free(mp);

  return(0);   // In Unix, return code of 0 means 'success'
}
