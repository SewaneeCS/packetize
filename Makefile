all: pckTest

pckTest: packetize.o pckTest.o
	gcc -o pckTest packetize.o pckTest.o

pckTest.o: pckTest.c
	gcc -Wall -c pckTest.c

packetize.o: packetize.c packetize.h
	gcc -Wall -c packetize.c

clean: 
	rm packetize.o pckTest.o packetize.c~ pckTest.c~


